<?php
/**
 * Created by PhpStorm.
 * User: nickd_000
 * Date: 3/05/2016
 * Time: 21:06
 */

class Config {

    private static $configInstantie = null;

    private $server;
    private $database;
    private $username;
    private $password;

    private function __construct()
    {
        $this->server = "localhost";
        $this->database = "mySite";
        $this->username = "root";
        $this->password = "";
    }

    public static function getConfigInstantie()
    {
        if(is_null(self::$configInstantie))
        {
            self::$configInstantie = new Config();
        }
        return self::$configInstantie;
    }

    public function getServer()
    {
        return $this->server;
    }
    public function getDatabase()
    {
        return $this->database;
    }
    public function getUsername()
    {
        return $this->username;
    }
    public function getPassword()
    {
        return $this->password;
    }

}