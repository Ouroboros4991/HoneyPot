<?php
/**
 *
 */
 require_once("config.php");
class database
{
    private static $databaseInstantie = null;
    private $db;
    function __construct()
    {
        try{
        $server = config::getConfigInstantie()->getServer();
        $database = config::getConfigInstantie()->getDatabase();
        $username = config::getConfigInstantie()->getUsername();
        $password = config::getConfigInstantie()->getPassword();
        $this->db = new PDO("mysql:host=$server; dbname=$database",$username,$password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e ){
        die($e->getMessage());
        }
    }

    public static function getInstantie(){
        if(is_null(self::databaseInstantie)){
            self::$databaseInstantie = new database();
        }
        return self::$databaseInstantie;
    }

    public function sluitdb(){
        $this->db = null;
    }

    public function getNumberAttemps($username){
        $sql = 'SELECT time FROM login_attemps where user_id = :user';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user',$username);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_CLASS);
        if(isset($result[0])){
                return $result[0]->time;
        }else{
            return null;
        }
    }

    public function createAttemp($username){
        $sql = 'Insert into login_attemps(user_id) values(:user)';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user',$username);
        $stmt->execute();
    }

    public function increaseAttemps($username){
        $sql = 'Update login_attemps set time = time + 1 where user_id = :user';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user',$username);
        $stmt->execute();

    }

    public function resetAttemps($username){
        $sql = 'Update login_attemps set time = 0 where user_id = :user';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user',$username);
        $stmt->execute();
    }

    public function getPassword($username){
        $sql = 'SELECT passPhrase FROM users where username = :user';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user',$username);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_CLASS);
        if(isset($result[0])){
                return $result[0]->passPhrase;
        }else{
            return null;
        }
    }

    public function getAdminRights($username){
        $sql = 'SELECT adminRights FROM users where username = :user';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user',$username);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_CLASS);
        if(isset($result[0])){
                return $result[0]->adminRights;
        }else{
            return null;
        }
    }
}


 ?>
