<?php
require_once('db.php');
require_once("private/token.php");


function showLoginHTML(){
        ?>
        <form action="" method="post">
        <?php
        $token = new token();
        $token->createTokenHTML();
        ?>

        <p><label for="username">Username: </label>
        <input type="text" id="username" name="username" /> </p>
        <p><label for="password">Password: </label>
            <input type="password" id="password" name="password"></p>
        <input type="submit" value="Login"/>
    </form>
    <?php

}

function handleLogin($username,$password){
    $db = new database();
    if(checkNumberAttemps($username,$db)){
        if(checkPassword($username,$password,$db)){
            $db->resetAttemps($username);
            $_SESSION['username'] = $username;
            $adminrights =  $db->getAdminRights($username);
            $_SESSION['adminrights'] = $adminrights;
            return true;
        }
    }
    $db->increaseAttemps($username);
    return false;

}

function checkPassword($username,$password,$db){
    $hash = $db->getPassword($username);
    if($hash != null){
        return true;
    }else{
        return false;
    }
}

function checkNumberAttemps($username,$db){
    $attemps = $db->getNumberAttemps($username);
    if($attemps != null ){
        if(!$attemps >= 0 && !$attemps <= 10){
            return true;
        }
    }else{
        $db->createAttemp($username);
    }
    return false;


}


 ?>
