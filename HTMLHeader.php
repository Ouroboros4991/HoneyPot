<?php
    header('Content-Security-Policy: default-src \'self\'');
    header('X-FRAME-OPTIONS','DENY');
    session_regenerate_id(true);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>HoneyPot</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
<header class="navbar navbar-bright navbar-fixed-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="/HoneyPot" class="navbar-brand">Home</a>
    </div>
    <nav class="collapse navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
        <?php if(isset($_SESSION['username']) && isset($_SESSION['adminrights']) ){
            echo '<li id="hidden_user">
                <a href="profile.php">Profile</a>
                </li>';
            }?>
          <?php if(isset($_SESSION['username']) && isset($_SESSION['adminrights']) && $_SESSION['adminrights'] === '1'){
              echo '<li id="hidden_admin">
                <a href="admin.php">Admin</a>
              </li>';
          }?>

      </ul>
      <dev class="nav navbar-right navbar-nav">
          <?php if(isset($_SESSION['username']) ){
             echo '<form class="form-inline" action="logout.php" method="post">
                <button type="submit" class="btn btn-default pull-right">Logout</button>
              </form>';
          }else{
              echo '<form class="form-inline" action="login.php" method="post">
                <button type="submit" class="btn btn-default pull-right">Login</button>
              </form>';
          }

         ?>

     </dev>
    </nav>
  </div>
</header>

<div id="masthead">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Groep 16 Honeypot
        </h1>
      </div>
      <div class="col-md-5">

      </div>
    </div>
  </div><!-- /cont -->



</div>
