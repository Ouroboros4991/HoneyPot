<?php
	require_once('private/utils.php');
	start_session();
	require_once('HTMLHeader.php');
    require_once('private/user.php');




    //TODO: check if user is logged in



 ?>

 <div class="container">
   <div class="row">
	 <div class="col-md-12">
	   <div class="top-spacer"> </div>
	 </div>
   </div>
 </div><!-- /cont -->

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-body">



          <!--/stories-->
          <div class="row">
            <div class="col-md-10 col-sm-9">
              <h2>Login</h2>
              <div class="row">
                  <form action="" method="post">
                      <?php
                      //TODO check if user is logged in
                      if(isset($_POST["username"])){
                          if(checkToken()){
                              $username = validateInput($_POST["username"]);
                              $password = validateInput($_POST["password"]);
                              if(handleLogin($username,$password)){
                                  header("Location: index.php");
                              }else{
                                  echo "error";
                              }
                          }


                      }
                      else{
                          showLoginHTML();
                      }?>
                     </form>
                <div class="col-xs-3"></div>
              </div>
              <br><br>
            </div>
          </div>
          <hr>


        </div>
      </div>
   	</div><!--/col-12-->
  </div>
</div>


 <?php
 	require_once('HTMLFooter.php');
  ?>
